# Conexion de APP Android con API de Mercado Libre

Esta es una aplicacion realizada con Flutter, que realiza una conexión con la API de Mercado Libre.
Puede descargar el [APK Debug](app-debug.apk) para revisar su instalacion.

# Contenido del Proyecto

Las carpetas creadas para el funcionamiento del proyecto son:

- **_assets_**: Contiene las imagenes propias de la aplicacion como el logo de la pantalla inicial y una imagen gif para la carga de imagenes externas.
- **_lib/src_**: Contine toda la estructura de la apliacacion dividia en sub-carpetas (models, pages, routes) que permite un mejor manejo de los documentos principales y una mejor forma de realizar el respectivo mantenimiento.
- **_lib/src/model_**: Contiene las clases que se utilizaran en la aplicacion. Este caso se creo la clase Product con los atributos index, nombre, valor, estado, ciudad e imagen.
- **_lib/src/pages_**: Contiene los documentos de las pantalla que se muestran en la aplicacion. Un documento por pantalla.
    - _home_page.dart_: es la pantalla inicial de la aplicacion.
    - _result_page.dart_: es la pantalla donde se muestran los resultados de la busqueda.
    - _detail_page.dart_: es la pantalla donde se muestra un producto especifico seleccionado en la pantalla anterior.
- **_lib/src/routes_**: Contiene el documento routes.dart en donde instancian las rutas que realiza la aplicacion para su correcta navegacion.
- **_lib/util/_**: Contiene documentos de utilidad para aplicacion:
    - _connection.dart_: Es la clase que se encargar en realizar la respectiva peticion _get_ a la Api de Mercado Libre, con una consulta especifica
    - _globals.dart_: Es el archivo encargado de guardar datos importante que se manejan de forma global en la aplicacion
    - _styles.dart_: Contiene funciones de diseño

![screen_shots](screen_shots/estructura.png)

# Funcionalidad
La aplicacion consta de 3 pantallas consecutivas donde cada pantalla depende de la anterior.

![screen_shots](screen_shots/pantalla_1.jpeg) ![screen_shots](screen_shots/pantalla_2.jpeg) ![screen_shots](screen_shots/pantalla_3.jpeg)

#### Pantalla Campo de Busqueda
En esta pantalla se realiza la verificacion si hay o no acceso a internet. Al no encontrar encontrar acceso a internet se muestra un mensaje inidcando que no puede acceder a internet. Esto se realiza por medio de la libreria **connectivity**
```dart
import 'package:connectivity/connectivity.dart';
void _getConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    var conn = getConnectionValue(result);
    setState(() {
      connectivityDevice = conn;
    });
  }
```
Si esta validacion es exitosa la siguiente validacion que se realiza es determinar que el campo de busqueda no este vacio. Si se quiere realizar una consulta 'vacia' la aplicacion no ejecutara ninguna accion. Al realizar una consulta valida (con un texto en el campo de busqueda), la aplicacion ejecutara la ruta _'result'_ y guarda la busqueda en la variable global **productSearched**
```dart
void _searchProduct() {
    _getConnectivity();
    if (connectivityDevice == 'None') {
        ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
            content: Text(
            "No hay acceso a internet",
            ),
        ),
        );
    } else {
        String _text = inpSearchController.text;
        if (_text != '') {
        globals.productSearched = _text;
        Navigator.pushNamed(context, 'results');
        }
    }
}
```
#### Visualizacion de resultados de la busqueda
En esta pantalla se realiza la conexion con la API por medio del metodo `getData()` de la clase `ConnectionServices`. Al realizar el consumo de la API el metodo convierte la respuesta a una lista del objeto `Product`
```dart
Future<List<Product>> getData() async {
    List<Product> _listResponse = [];
    var url =
        Uri.parse("https://api.mercadolibre.com/sites/MCO/search?q=$text#json");
    final response = await http.get(url);
    if (response.statusCode == 200) {
      String body = utf8.decode(response.bodyBytes);
      final jsonData = jsonDecode(body);
      var arr = jsonData['results'];
      for (int i = 0; i < arr.length; i++) {
        _listResponse.add(new Product(
            i,
            arr[i]['title'],
            arr[i]['price'],
            arr[i]['address']['state_name'],
            arr[i]['address']['city_name'],
            arr[i]['thumbnail']));
      }
      print(jsonData);
      return _listResponse;
    } else {
      throw Exception("Fallo la conexion");
    }
  }
```
En la visualizacion de la informacion se realiza por medio de un `ListView` que contiene una `Card` por cada elemento devuelto del servicio, se realiza el formato de los precios con separador de miles.
```dart
List<Widget> _listViewResult(List? data) {
    var f = NumberFormat('#,##0', 'es_CO');
    List<Widget> productos = [];
    for (var producto in data!) {
      var valor = f.format(producto.valor);
      final widgetTemp = Card(
        elevation: 10.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Image.network(
                producto.imagen,
                width: 80.0,
              ),
              title: Text('\$ $valor'),
              subtitle: Text(producto.nombre),
              onTap: () {
                _imprimir(producto);
              },
            ),
          ],
        ),
      );
      productos.add(widgetTemp);
    }
    return productos;
  }
```
#### Detalle del Producto
Al realizar tap en alguna de las cards de la pantalla de visualizacion de resultados nos ejecuta la ruta `'detail'` que muestra los datos a detalle del producto junto con su fotografia. Estos valores los muestra por medio del objeto global `productSelected` que se almacena en el momento de realizar el tap de las cards anteriores.

```dart
Widget _createInformationProductDescription() {
    var f = NumberFormat('#,##0', 'es_CO');
    var valor = f.format(globals.productSelected.valor);
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomRight,
          padding: EdgeInsets.only(
            top: 10.0,
            left: 10.0,
          ),
          child: Row(
            children: [
              Icon(
                Icons.location_on_outlined,
                color: Colors.grey,
              ),
              Text(
                  '${globals.productSelected.estado} - ${globals.productSelected.ciudad}'),
            ],
          ),
        ),
        Container(
          alignment: Alignment.topLeft,
          padding: EdgeInsets.all(10.0),
          child: Text(
            '\$ $valor',
            style: TextStyle(
              fontFamily: 'OpenSans',
              fontSize: 25.0,
            ),
          ),
        ),
      ],
    );
  }
```

