class Product {
  int index = 0;
  String nombre = "";
  int valor = 0;
  String estado = "";
  String ciudad = "";
  String imagen = "";

  Product(int index, String nombre, int valor, String estado, String ciudad,
      String imagen) {
    this.index = index;
    this.nombre = nombre;
    this.valor = valor;
    this.estado = estado;
    this.ciudad = ciudad;
    this.imagen = imagen;
  }

  Product.setDefault() {
    this.index = 0;
    this.nombre = "";
    this.valor = 0;
    this.estado = "";
    this.ciudad = "";
    this.imagen = "";
  }
}
