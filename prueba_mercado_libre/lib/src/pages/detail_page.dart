import 'package:flutter/material.dart';
import 'package:prueba_mercado_libre/src/models/product.dart';
import 'package:prueba_mercado_libre/util/globals.dart' as globals;
import 'package:intl/intl.dart';

class DetailPage extends StatefulWidget {
  DetailPage({Key? key}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  void dispose() {
    globals.productSelected = new Product.setDefault();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.blue[900]),
        backgroundColor: Color(0xFFFFF159),
        title: Text(
          'Descripción',
          style: TextStyle(color: Colors.blue[900]),
        ),
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          padding: EdgeInsets.all(20.0),
          width: double.infinity,
          child: Column(
            children: [
              _createCardDetail(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _createCardDetail() {
    final card = Container(
      child: Column(
        children: [
          _createInformationProductTitle(),
          FadeInImage(
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 500),
            height: 400.0,
            fit: BoxFit.cover,
            image: NetworkImage(globals.productSelected.imagen),
          ),
          _createInformationProductDescription(),
        ],
      ),
    );

    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: card,
      ),
      decoration: BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              spreadRadius: 2.0,
              offset: Offset(2.0, 10.0))
        ],
      ),
    );
  }

  Widget _createInformationProductTitle() {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Text(
        globals.productSelected.nombre,
        style: TextStyle(
          fontFamily: 'OpenSans',
          fontSize: 12.0,
        ),
      ),
    );
  }

  Widget _createInformationProductDescription() {
    var f = NumberFormat('#,##0', 'es_CO');
    var valor = f.format(globals.productSelected.valor);
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomRight,
          padding: EdgeInsets.only(
            top: 10.0,
            left: 10.0,
          ),
          child: Row(
            children: [
              Icon(
                Icons.location_on_outlined,
                color: Colors.grey,
              ),
              Text(
                  '${globals.productSelected.estado} - ${globals.productSelected.ciudad}'),
            ],
          ),
        ),
        Container(
          alignment: Alignment.topLeft,
          padding: EdgeInsets.all(10.0),
          child: Text(
            '\$ $valor',
            style: TextStyle(
              fontFamily: 'OpenSans',
              fontSize: 25.0,
            ),
          ),
        ),
      ],
    );
  }
}
