import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:prueba_mercado_libre/src/models/product.dart';
import 'package:prueba_mercado_libre/util/connection.dart';
import 'package:prueba_mercado_libre/util/globals.dart' as globals;
import 'package:intl/intl.dart';

class ResultPage extends StatefulWidget {
  ResultPage({Key? key}) : super(key: key);

  @override
  _ResultPageState createState() => _ResultPageState();
}

class _ResultPageState extends State<ResultPage> {
  ConnectionServices conenection =
      new ConnectionServices(globals.productSearched);
  Future<List<Product>>? _listaProductos;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _listaProductos = conenection.getData();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onReturnView,
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.blue[900]),
          backgroundColor: Color(0xFFFFF159),
          title: Text(
            'Resultados',
            style: TextStyle(color: Colors.blue[900]),
          ),
        ),
        body: _createListResults(),
      ),
    );
  }

  Future<bool> _onReturnView() {
    return new Future(() {
      globals.productSearched = "";
      Navigator.of(context).pushNamed('/');
      return false;
    });
  }

  Widget _createListResults() {
    return FutureBuilder(
      future: _listaProductos,
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        if (snapshot.hasData) {
          return ListView(
            children: _listViewResult(snapshot.data),
          );
        } else if (snapshot.hasError) {
          return Text("Erro de carga");
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  List<Widget> _listViewResult(List? data) {
    var f = NumberFormat('#,##0', 'es_CO');
    List<Widget> productos = [];
    for (var producto in data!) {
      var valor = f.format(producto.valor);
      final widgetTemp = Card(
        elevation: 10.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Image.network(
                producto.imagen,
                width: 80.0,
              ),
              title: Text('\$ $valor'),
              subtitle: Text(producto.nombre),
              onTap: () {
                _imprimir(producto);
              },
            ),
          ],
        ),
      );
      productos.add(widgetTemp);
    }
    return productos;
  }

  void _imprimir(Product producto) {
    globals.productSelected = producto;
    Navigator.pushNamed(context, 'detail');
  }
}
