import 'package:flutter/material.dart';
import 'package:prueba_mercado_libre/util/styles.dart';
import 'package:prueba_mercado_libre/util/globals.dart' as globals;
import 'package:connectivity/connectivity.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController inpSearchController = TextEditingController();
  String connectivityDevice = '';

  @override
  void initState() {
    super.initState();
    inpSearchController = TextEditingController();
    _getConnectivity();
  }

  @override
  void dispose() {
    super.dispose();
    inpSearchController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _createBackground(),
          _createStructure(),
        ],
      ),
    );
  }

  Widget _createBackground() {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xFFFFF159),
          ],
          stops: [0.9],
        ),
      ),
    );
  }

  Widget _createStructure() {
    return Container(
      height: double.infinity,
      child: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        padding: EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 100.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildLogoImage(),
            _buildForm(),
          ],
        ),
      ),
    );
  }

  Widget _buildLogoImage() {
    return Column(
      children: [
        Container(
          width: double.infinity,
          child: Image.asset('assets/mercado_libre_logo.png'),
        ),
        SizedBox(
          height: 10.0,
        )
      ],
    );
  }

  Widget _buildForm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _createInputSearch(),
        _createButtonSearch(),
      ],
    );
  }

  Widget _createInputSearch() {
    return Container(
      alignment: Alignment.centerLeft,
      decoration: decorationContainerInputYellow,
      height: 60.0,
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.only(top: 14.0),
          prefixIcon: Icon(
            Icons.shopping_cart,
            color: Colors.grey[800],
          ),
          hintText: 'Buscar productos, marcas y mas...',
          hintStyle: stylePlaceholder,
        ),
        controller: inpSearchController,
      ),
    );
  }

  Widget _createButtonSearch() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      width: double.infinity,
      child: ElevatedButton.icon(
        icon: Icon(
          Icons.search,
          color: Colors.grey[700],
        ),
        style: styleButton,
        label: Text(
          'Buscar',
          style: TextStyle(
            fontFamily: 'OpenSans',
            fontSize: 14.0,
            color: Colors.grey[700],
          ),
        ),
        onPressed: () => _searchProduct(),
      ),
    );
  }

  void _searchProduct() {
    _getConnectivity();
    if (connectivityDevice == 'None') {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "No hay acceso a internet",
          ),
        ),
      );
    } else {
      String _text = inpSearchController.text;
      if (_text != '') {
        globals.productSearched = _text;
        Navigator.pushNamed(context, 'results');
      }
    }
  }

  void _getConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    var conn = getConnectionValue(result);
    setState(() {
      connectivityDevice = conn;
    });
  }

  String getConnectionValue(var connectivityResult) {
    String status = '';
    switch (connectivityResult) {
      case ConnectivityResult.mobile:
        status = 'Mobile';
        break;
      case ConnectivityResult.wifi:
        status = 'Wi-Fi';
        break;
      case ConnectivityResult.none:
        status = 'None';
        break;
      default:
        status = 'None';
        break;
    }
    return status;
  }
}
