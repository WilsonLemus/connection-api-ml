import 'package:flutter/material.dart';
import 'package:prueba_mercado_libre/src/pages/detail_page.dart';

import 'package:prueba_mercado_libre/src/pages/home_page.dart';
import 'package:prueba_mercado_libre/src/pages/result_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => HomePage(),
    'results': (BuildContext context) => ResultPage(),
    'detail': (BuildContext context) => DetailPage(),
  };
}
