import 'package:flutter/material.dart';

final decorationContainerInputYellow = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(10.0),
    boxShadow: [
      BoxShadow(color: Colors.grey, blurRadius: 6.0, offset: Offset(0, 2))
    ]);

final stylePlaceholder = TextStyle(
  color: Colors.grey,
  fontFamily: 'OpenSans',
);

final styleButton = ElevatedButton.styleFrom(
  primary: Colors.grey.shade100,
  onPrimary: Colors.grey,
  padding: EdgeInsets.all(15.0),
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(10.0),
  ),
);
