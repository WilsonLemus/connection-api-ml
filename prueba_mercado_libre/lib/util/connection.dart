import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:prueba_mercado_libre/src/models/product.dart';

class ConnectionServices {
  String text = "";

  ConnectionServices(String text) {
    this.text = text;
  }

  Future<List<Product>> getData() async {
    List<Product> _listResponse = [];
    var url =
        Uri.parse("https://api.mercadolibre.com/sites/MCO/search?q=$text#json");
    final response = await http.get(url);
    if (response.statusCode == 200) {
      String body = utf8.decode(response.bodyBytes);
      final jsonData = jsonDecode(body);
      var arr = jsonData['results'];
      for (int i = 0; i < arr.length; i++) {
        _listResponse.add(new Product(
            i,
            arr[i]['title'],
            arr[i]['price'],
            arr[i]['address']['state_name'],
            arr[i]['address']['city_name'],
            arr[i]['thumbnail']));
      }
      print(jsonData);
      return _listResponse;
    } else {
      throw Exception("Fallo la conexion");
    }
  }
}
